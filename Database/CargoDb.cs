using CreonitCargo.Models;
using Microsoft.EntityFrameworkCore;

namespace CreonitCargo;

interface ICargoDB
{
    public DbSet<ArtefactTable> Artefacts { get; set; }
    public DbSet<ProjectTable> Projects { get; set; }
}

public class CargoDb : DbContext, ICargoDB
{
    public DbSet<ArtefactTable> Artefacts { get; set; }
    public DbSet<ProjectTable> Projects { get; set; }

    public string DbPath { get; }

    public CargoDb()
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var projects = modelBuilder.Entity<ProjectTable>();
        projects.HasKey(p => p.Title);
        projects.HasMany(e => e.Artefacts).WithOne(e => e.Project);
        
        var artefacts = modelBuilder.Entity<ArtefactTable>();
        artefacts.HasKey(p => p.ID);
        base.OnModelCreating(modelBuilder);
    }

    // The following configures EF to create a Sqlite database file in the
    // special "local" folder for your platform.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseNpgsql(
            $"Host=postgres;" +
            $"Database=cargo;" +
            $"Username=user;" +
            $"Password=akdbY31EoS9b");
}