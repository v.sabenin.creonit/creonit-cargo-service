namespace CreonitCargo.DTOs;

public class ProjectDTO
{
    public String Title { get; set; }
    public String IconURL { get; set; }
}