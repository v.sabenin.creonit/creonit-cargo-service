namespace CreonitCargo.DTOs;

public class ArtefactDTO
{
    public int ID { get; set; }
    public ProjectDTO Project { get; set; }
    public string Environment { get; set; }
    public string Version { get; set; }
    public int Build { get; set; }
}