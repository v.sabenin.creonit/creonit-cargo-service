namespace CreonitCargo.Models;

public class ArtefactTable
{
    public int ID { get; set; }
    public String ArtefactPath { get; set; }
    public String Version { get; set; }
    public string Environment { get; set; }
    public DateTimeOffset PublicationTimestamp { get; set; }
    public int Build { get; set; }
    
    public ProjectTable Project { get; set; }
}