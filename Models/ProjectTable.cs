namespace CreonitCargo.Models;

public class ProjectTable
{
    public String Title { get; set; }
    public String Icon { get; set; }
    public List<ArtefactTable> Artefacts { get; set; }
}