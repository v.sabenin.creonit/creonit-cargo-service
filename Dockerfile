﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["CreonitCargo.csproj", "./"]
RUN dotnet restore "CreonitCargo.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "CreonitCargo.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "CreonitCargo.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY ["./wwwroot/", "/app/wwwroot"]
ENTRYPOINT ["dotnet", "CreonitCargo.dll"]
