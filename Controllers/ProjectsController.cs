using CreonitCargo.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CreonitCargo.Controllers;


[ApiController]
[Route("projects")]
public class ProjectsController
{
    private readonly ILogger<ArtefactsController> _logger;
    private readonly CargoDb _cargoDb;

    public ProjectsController(CargoDb db, ILogger<ArtefactsController> logger)
    {
        _cargoDb = db;
        _logger = logger;
    }
    
    [HttpGet]
    public async Task<IEnumerable<ProjectDTO>> GetProjects()
    {
        var artefacts = await _cargoDb.Projects.ToListAsync();
        return artefacts.Select(e => new ProjectDTO()
        {
            Title = e.Title,
            IconURL = e.Icon,
        });
    }

}