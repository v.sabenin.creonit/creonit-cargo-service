using System.IO.Pipelines;
using CreonitCargo.DTOs;
using CreonitCargo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CreonitCargo.Controllers;

[ApiController]
[Route("artefacts")]
public class ArtefactsController : ControllerBase
{
    private readonly ILogger<ArtefactsController> _logger;
    private readonly CargoDb _cargoDb;
    private int maxPerProjectArtefactsCount = 9;
    private string artefactsPath = "/data/artefacts";

    public ArtefactsController(CargoDb db, ILogger<ArtefactsController> logger)
    {
        _cargoDb = db;
        _logger = logger;
    }

    async Task<ProjectTable> GetProjectByProject(String projectName)
    {
        var project = await _cargoDb.Projects.SingleOrDefaultAsync(e => e.Title == projectName);
        if (project == default)
        {
            project = new ProjectTable
            {
                Title = projectName,
                Icon = "",
            };
            _cargoDb.Projects.Add(project);
            await _cargoDb.SaveChangesAsync();
        }

        return project;
    }

    [Route("download/{**page}")]
    [HttpGet]
    public async Task GetArtefact([FromQuery] int artefactId)
    {
        var artefact = await _cargoDb.Artefacts.SingleOrDefaultAsync(e => e.ID == artefactId);
        if (artefact == default)
        {
            throw new ArgumentException("ID not found");
        }

        Response.ContentType = "application/octet-stream";
        Response.ContentLength = new FileInfo(artefact.ArtefactPath).Length;
        var reader = new StreamReader(artefact.ArtefactPath);
        await reader.BaseStream.CopyToAsync(Response.Body);
    }


    [HttpGet]
    public async Task<IEnumerable<ArtefactDTO>> GetArtefacts([FromQuery] string projectId)
    {
        var artefacts = await _cargoDb.Artefacts.Include(e => e.Project).Where(e => e.Project.Title == projectId)
            .OrderByDescending(e => e.PublicationTimestamp)
            .ToListAsync();
        return artefacts.Select(e => new ArtefactDTO
        {
            Project = new ProjectDTO()
            {
                Title = e.Project.Title,
                IconURL = e.Project.Icon,
            },
            ID = e.ID,
            Build = e.Build,
            Version = e.Version,
            Environment = e.Environment,
        });
    }

    /*[Route("download")]
    [HttpGet]
    public async Task<String> Upload([FromQuery] string projectId, [FromQuery] string ver,
        [FromForm] IFormFile artefact)
    {
        var artefactPath = "./artefacts/" + artefact.FileName;
        var writer = new StreamWriter(artefactPath);
        await artefact.CopyToAsync(writer.BaseStream);
        await writer.FlushAsync();

        var project = await GetProjectByProject(projectId);
        var newArtefact = new ArtefactTable
        {
            ArtefactPath = artefactPath,
            Version = ver,
            Project = project
        };
        _cargoDb.Artefacts.Add(newArtefact);
        await _cargoDb.SaveChangesAsync();
        
        return $"Artefact {artefact.FileName} [{artefact.Length / 1_000_000}] uploaded ";
    }*/

    public async Task DeleteOutdatedArtefacts(string projectId)
    {
        var artefacts = await _cargoDb.Artefacts.Where(e => e.Project.Title == projectId)
            .OrderByDescending(p => p.PublicationTimestamp)
            .Skip(maxPerProjectArtefactsCount).ToListAsync();

        foreach (var p in artefacts)
        {
            _cargoDb.Remove(p);
            System.IO.File.Delete(p.ArtefactPath);
        }

        await _cargoDb.SaveChangesAsync();
    }


    [Route("upload")]
    [HttpPost]
    [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
    public async Task<String> Upload([FromQuery] string projectId, [FromQuery] string ver,
        [FromQuery] string environment,
        [FromQuery] int build,
        [FromForm] IFormFile artefact)
    {
        await DeleteOutdatedArtefacts(projectId);

        var artefactFileName = $"{projectId}_{ver}_{build}_{environment}.apk";
        var artefactPath = Path.Combine(artefactsPath, artefactFileName);
        var writer = new StreamWriter(artefactPath);
        await artefact.CopyToAsync(writer.BaseStream);
        await writer.FlushAsync();

        var project = await GetProjectByProject(projectId);
        var newArtefact = new ArtefactTable
        {
            ArtefactPath = artefactPath,
            Version = ver,
            Project = project,
            Build = build,
            Environment = environment,
            PublicationTimestamp = DateTimeOffset.Now,
        };
        _cargoDb.Artefacts.Add(newArtefact);
        await _cargoDb.SaveChangesAsync();

        return $"Artefact {artefact.FileName} [{artefact.Length / 1_000_000}] uploaded ";
    }
}